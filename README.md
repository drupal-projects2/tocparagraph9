tocparagraph9
======

A drupal 9 example to show a TOC (table of content) in nodes. 

Requirements
------------

- Webserver to run Drupal
- Composer (to install Drupal)
- Drupal 9

Installation
------------
Download this repo with git and install it as usual, see https://www.drupal.org/docs/develop/using-composer/manage-dependencies for further information.

After Drupal installation install the configuration file inside the folder config/site/
this configuration is all the config this example use.

Configuration
-------------

This TOC example is already config to see it. But you can change content type configuration inside Article and you can change Tocify module block going inside block configuration to choose class to watch (context) and Selectors to slect any h2, h3 or more selectors.

Repository
-------
https://gitlab.com/drupal-projects2/tocparagraph9

Contact
-------

Current maintainers

  Ivan Mejia https://www.drupal.org/u/koffer
  

